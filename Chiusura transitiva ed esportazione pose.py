# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <headingcell level=1>

# Chiusura transitiva ed esportazione pose 

# <headingcell level=2>

# Detection

# <codecell>

import numpy as np
from pytable.orchestra import *
import glob
filelist=glob.glob('data/images/*.jpg')
filelist=[fn.split('/')[-1] for fn in filelist]
filelist.sort()
for filename in filelist:
    tab.addImage(filename)

detectorSize=4000
detector_label='ORB{}'.format(detectorSize)
Ibins=tab.allImages()
detect_orchestra(Ibins,detector_label)
M0=match_uno(len(Ibins),1,3)
M0bins=[tab.getMatch(Ii, Ij) for Ii,Ij in M0]

# <headingcell level=2>

# Matching

# <codecell>

match_orchestra(M0bins,detector_label)

# <headingcell level=2>

# Chiusura transitiva

# <codecell>

import pytable.points as pts

# <headingcell level=3>

# Calcolo

# <rawcell>

# La chiusura transitiva è calcolata da una lista di matchBin e una di imageBin

# <codecell>

tracks=pts.closure(M0bins,Ibins)

# <headingcell level=3>

# Scrittura su file

# <codecell>

la funzione export in pytable.points permette di salvare un file per ciascun punto da descrivere

# <codecell>

pts.export(tracks)

# <codecell>

cd data/points/

# <codecell>

ls

# <rawcell>

# Il formato di ciascun file è <id-immagine> <id-keypoint> <u> <v>
# in pytable/points.py :"{} {} {} {}\n".format(image_id,v.id_keypoint,v.v,v.u) 

# <headingcell level=3>

# Scrematura

# <rawcell>

# il metodo prune permette di escludere i punti che non sono visti da un numero minimo di immagini. Nell' esempio il minimo è 4

# <codecell>

tracks.prune(4)

# <headingcell level=1>

# Scrivere su File le Pose

# <codecell>

import pytable.poseObservation as obs

# <codecell>

obs.export(M0bins)

# <rawcell>

# la funzione pytable.poseObservation.export scrive su file la matrice fondamentale , quella di rotazione e il vettore di traslazione osservati su coppie di immagini.
# Ciascuna coppia è rappresentata da un file, il cui formato è definito in poseObservation.py:
# <F>
# <R>
# <T>
# <numero di corrispondenze puntuali>
# <nome della prima immagine>
# <nome della seconda immagine>
#     
# I file sono salvati in data/poseObservations

# <codecell>

cd data/poseObservations/

# <codecell>

ls

# <codecell>


