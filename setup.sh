

sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
echo "deb http://downloads.skewed.de/apt/trusty trusty universe" | sudo tee -a /etc/apt/sources.list
echo "deb-src http://downloads.skewed.de/apt/trusty trusty universe" | sudo tee -a /etc/apt/sources.list
sudo apt-get update
sudo apt-get -y install python-graph-tool
sudo apt-get -y install libboost-all-dev
sudo apt-get -y install python-setuptools
sudo easy_install smop
#controllare presenza ipython
#controllare presenza numpy
#controllare presenza dgraph tools
#controllare presenza opencv
sudo apt-get -y install python-pip
sudo pip install pyzmq
sudo pip install ipython[all]
sudo apt-get -y install ipython-notebook
ipython -c 'import numpy,graph_tool,cv2'
cd kgraph/
make -j8
cd python/
make -j8
cd ..
cd ..
sudo cp kgraph/bin/libkgraph.so /usr/local/lib
cp kgraph/bin/libkgraph.so pytable/
cp kgraph/python/pykgraph.so pytable/
sudo ldconfig


cd hamming/
python setup.py build
cd ..
cp hamming/build/*/*/hamming.so pytable/
ipython -c 'import pytable,pytable.hamming,pytable.pykgraph'
ipython -c 'import pytable,pytable.matchBin,pytable.imageBin'
ipython -c 'import pytable,pytable.transclosure'

