# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <headingcell level=1>

# Funzionamento Base

# <rawcell>

# Si importano i moduli necessari

# <codecell>

import numpy as np

# <rawcell>

# orchestra contiene alcune funzioni per lo step di detection e quello di matching

# <codecell>

from pytable.orchestra import *

# <rawcell>

# Glob è utile per listare il contenuto delle directory

# <codecell>

import glob

# <rawcell>

# selezioniamo le immagini jpg in data/images/ avendo cura di preservare l' ordine di nome

# <codecell>

filelist=glob.glob('data/images/*.jpg')
filelist=[fn.split('/')[-1] for fn in filelist]
filelist.sort()

# <rawcell>

# in orchestra è definito tab  

# <codecell>

tab=TABLE.MatchTable()

# <rawcell>

# MatchTable è la classe che regge l' impianto di book-keeping
# MatchTable mantiene una lista di contenitori-immagine (imageBin) e per ciascuna coppia di questi un contenitore-coppia (matchBin).
# La tavola si popola con il comando tab.addImage(filename)

# <codecell>

for filename in filelist:
    tab.addImage(filename)

# <rawcell>

# Pytable supporta raccolte multiple di keypoint e feature (es una lista di keypoint e feature sift e una orb). Per distinguere le diverse collezzioni serve un etichetta che identifichi la procedura di detection (che in senso largo comprende la scelta dei punti angolosi, la loro descrizione e l' indicizzazione delle descrizioni)

# <codecell>

detectorSize=4000
detector_label='ORB{}'.format(detectorSize)

# <headingcell level=2>

# Detection

# <codecell>

Ibins=tab.allImages()
detect_orchestra(Ibins,detector_label)

# <markdowncell>

# in orchestra viene definito il processo remoto che opera su ciascuna immagine:

# <codecell>

def detectProcess_remote(request):
    I,label,n,callback_id=request
    #opencv detector are responsible of both keypoint detection and description
    detector=cv2.ORB(n,nlevels=32)
    #detect,describe and compute k-nn index of image features
    I.detect_and_train(detector,label)
    return (callback_id,I.kp[label])

# <markdowncell>

# detect_orchestra usa un load balancer che delega l' esecuzione di detectProcess_remote ad altri processi (detti engine o cluster). Questi salvano i loro risultati su file e notificano il controller quando hanno terminato l' esecuzione. Il controller usa detectProcess_callback per elaborare la notifica.

# <headingcell level=3>

# File salvati su disco

# <codecell>

cd data/

# <codecell>

ls

# <headingcell level=4>

# Feat

# <markdowncell>

# i punti vengono salvati nel formato binario di numpy in feat/nome_immagine/detector_label.npz.
# I keypoint sono salvati come vettori in virgola fissa, le feature sift o surf come vettori in virgola mobile , le feature brief come vettori di interi privi di segno (rispettivament np.float, np.float, np.uint8). 
# All' esterno della tavola i file npz possono essere caricati usando numpy.load(nomefile)

# <codecell>

cd feat/

# <codecell>

ls

# <codecell>

cd 1416841024555_MEP_IMAGE.jpg/

# <codecell>

ls

# <headingcell level=4>

# Indici Knn

# <codecell>

Gli indici Knn sono realizzati con Kgraph [http://www.kgraph.org/]  salvati in index/nome_immagine/detector_label.
Kgraph provvede ad un interfaccia per aprire e interrogare i file di indice sia in un ambiente python che c++.

# <codecell>

cd index/

# <codecell>

ls

# <codecell>

cd 1416841024555_MEP_IMAGE.jpg/

# <codecell>

ls

# <headingcell level=2>

# Scelta iniziale dei match da provare : windowed

# <codecell>

M0=match_uno(len(Ibins),1,4)

# <markdowncell>

# match_uno(n_images,k1,k2) costruisce un piano di match da calcolare "windowed": ciascun immagine è confrontata con le altre dalla sua k1-esima successiva fino alla k2-esima successiva. Se ciascun immagine è confrontata con (k2-k1) immagini successive è confrontata con (k2-k1) immagini precedenti. 

# <codecell>

print M0

# <codecell>

M0bins=[tab.getMatch(Ii, Ij) for Ii,Ij in M0]
match_orchestra(M0bins,detector_label)

# <headingcell level=3>

# File salvati su disco

# <rawcell>

# Per ciascuna coppia di immagini viene creato un archivio di match in match/etichetta/match{i}x{j}.npz.
# Tali file contengono un vettore di coppie di indici di keypoint. 
# Esempio: in match{i}x{j}.npz la coppia (a,b) indica che l' a-esimo keypoint dell' immagine i è stato riconosciuto nell' b-esimo keypont dell' immagine j.
#     

# <codecell>

ls

# <codecell>

cd match/

# <codecell>

ls

# <codecell>

cd ORB4000k4p40r2.0/

# <codecell>

ls

# <codecell>


