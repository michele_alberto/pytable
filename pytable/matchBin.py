from settings import paths
import cv2
import numpy as np
import weakref as wr
import matchFilter as mf
from hamming import hamming
import os.path

def skew(v):
        """
        compute skew symmetric matrix
        thanks to http://pythonpath.wordpress.com/2012/09/04/skew-with-numpy-operations/
        """
        if len(v) == 4: v = v[:3]/v[3]
        skv = np.roll(np.roll(np.diag(v.flatten()), 1, 1), -1, 0)
        return skv - skv.T

def F_from_poses(
        R_source,T_source,
        R_destination,T_destination,
        K_source,K_destination=None):
    #calculate rotation matrices from rodrigues angles
    Rmat_source=cv2.Rodrigues(R_source)[0]
    Rmat_destination=cv2.Rodrigues(R_destination)[0]
    K_s=K_source
    K_d=K_destination if type(K_destination)==np.ndarray else K_s
    Rmat_sd=np.dot(Rmat_source.T,Rmat_destination)
    T_sd=np.dot(Rmat_source.T, (T_destination-T_source))
    #rotation Rmat_sd and translation T_sd describe the rototranslation from source to destination
    #have a look 
    #@ Multiple View Geometry in Compurer Vision
    #A4.3 page 582
    #here we are calculating 9.4 at page 244
    #f=K'- T RK T [e] x
    skew_mat=skew(np.dot(
        np.dot(K_s,Rmat_sd.T),
        T_sd))
    b=np.dot(K_s,skew_mat)
    a=np.dot(
        np.linalg.inv(K_d.T),
        Rmat_sd)
    F=np.dot(a,b)
    return F

class MatchBin(object):
    def __init__(self, table, Ii, Ij):
        self._table = table
        self.Ii = Ii.index
        self.Ij = Ij.index
        self.matches = {}
        self._matchCache={}
        self.edge=None
        self.nodes=(Ii.node,Ij.node)

    def dump(self):
        model = {}
        model["ij"] = (self.Ii,self.Ij)
        model["matches"]=self.matches
        return model
    
    @staticmethod
    def load(table,model):
        i,j=model["ij"]
        m=MatchBin(table,table.getImage(i),table.getImage(j))
        m.matches=model["matches"]
        for ma in m.matches:
            assert(m.matches[ma].has_key('matchFile'))
            assert(os.path.isfile(m.matches[ma]['matchFile']))
        return m

    def n(self,label=None):
        match_label=self._infer_label(label)
        if match_label==None:
            return 0
        return self.matches[match_label]['n']
    def I(self):
        return self._table.getImage(self.Ii)
    def J(self):
        return self._table.getImage(self.Ij)
    def mwrite(self, label, matches):
        assert(not self.matches.has_key(label))
        match_array=np.array(matches)
        path=paths.matchDir + label + '/'
        fileName="match{}x{}.npz".format(self.Ii,self.Ij)
        fullName=path+fileName
        paths.ensureDir(path)
        np.savez(fullName, match_array=match_array)
        o={'matchFile':fullName,'n':len(match_array)}
        self.matches[label] = o
        return o
    def _infer_label(self,in_label=None):
        if in_label is not None:
            return in_label
        elif len(self.matches)>0:
            return list(self.matches)[0]
        else :
            return None
    def mread(self,match_label=None):
        label=self._infer_label(match_label)
        if label==None:
            return None
        assert(self.matches.has_key(label))
        npzfile =np.load( self.matches[label]['matchFile'] )
        matches=npzfile['match_array']
        return matches

    def computeFMatrix(self):
        s_geom=self.getImage(self.Ii).geometry
        d_geom=self.getImage(self.Ij).geometry
        if s_geom==None or d_geom==None :
            return None
        R_source=s_geom[1]
        T_source=s_geom[2]
        K_source=s_geom[0]
        R_destination=d_geom[1]
        T_destination=d_geom[2]
        K_destination=d_geom[0]
        self.F=F_from_poses(R_source,T_source,R_destination,T_destination,K_source,K_destination)
        return self.F

    def getF(self):
        if self.has('F'):
            return self.F
        self.computeFMatrix()
        return F
       

    def qualityInspection(self,matches,label,F):
        # Get images
        imI=self._table.getImage(self.Ii)
        imJ=self._table.getImage(self.Ij)
        # Get key points & Features 
        kpI=imI.kpread(label)
        kpJ=imJ.kpread(label)
        featI=imI.ftread(label)
        featJ=imJ.ftread(label)
        columnI=np.array([featI[match[0] ] for match in matches])
        columnJ=np.array([featJ[match[1] ] for match in matches])
        dtype=featI.dtype
        assert(featI.dtype==featJ.dtype)
        if dtype==np.uint8:
            columnI.dtype=np.uint64
            columnJ.dtype=np.uint64
            distance=sum(hamming(columnI,columnJ).T)
        elif dtype in [np.float,np.float32,np.float64]:
            distance=sum(np.square((columnI-columnJ)).T)
        else:
            assert(dtype in [np.float,np.uint8])
        homogeneusI=cv2.convertPointsToHomogeneous(kpI)
        homogeneusJ=cv2.convertPointsToHomogeneous(kpJ)
        Ferrors=[]
        for it,m in enumerate(matches):
            i=m[0]
            j=m[1]
            hi=homogeneusI[i]
            hj=homogeneusJ[j]
            Ferr=np.dot(hi ,np.dot( F , hj.T ))
            Ferrors.append(Ferr)
        Ferrors=np.array(Ferrors)
        return distance,Ferrors.flatten()

    def visualize_match(self,matches,descriptorLabel=None):
        if len(matches)==0 :
            return None
        imI=self._table.getImage(self.Ii)
        imJ=self._table.getImage(self.Ij)
        kpI=imI.kpread(descriptorLabel)
        kpJ=imJ.kpread(descriptorLabel)
        return mf.explore_match(matches,kpI,kpJ,imI.imread(),imJ.imread())
    
    def vsfm_style_export(self,match_label=None,descriptor_label=None):
        matches=self.mread(match_label)
        imI=self._table.getImage(self.Ii)
        imJ=self._table.getImage(self.Ij)
        kpI=imI.kpread(descriptor_label)
        kpJ=imJ.kpread(descriptor_label)
        headingI='{} {}\n'.format(imI.index,imI.imageFile())
        headingJ='{} {}\n'.format(imJ.index,imJ.imageFile())
        n=len(matches)
        ans=headingI+headingJ+("{}\n".format(n))
        for m in matches:
            mi=m[0]
            mj=m[1]
            kpi=kpI[mi]
            kpj=kpJ[mj]
            ix,iy=kpi[0],kpi[1]
            jx,jy=kpj[0],kpj[1]
            line="{} {} {} {} {} {}\n".format(mi,ix,iy,mj,jx,jy)
            ans=ans+line
        return ans
    def labels(self):
        return list(self.matches)
    def __str__(self):
        labelList=','.join(['#{}:{}'.format(l,self.matches[l]['n']) for l in self.labels()])
        return "MatchBin<{},{}> <{}>".format(self.Ii,self.Ij,labelList)
        
        
              
    

 
              
   
   
                
        
                    
        
        

