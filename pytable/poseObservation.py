from settings import paths
import zisserman
def export(mbinList):
    for m in mbinList:
        if m.n()>0 :
            if m.F is not None:
                I=m.I()
                J=m.J()
                fileName='{}x{}.txt'.format(m.Ii,m.Ij)
                #filename='{}x{}.txt'.format(I.imageName,J.imageName)
                path=paths.poseObservationDir
                fullName=path+fileName
                paths.ensureDir(path)
                with file(fullName,'w') as fo:
                    F=' '.join([str(f) for f in list(m.F.flatten())])+'\n'
                    R,T=zisserman.vgg_RT_from_F(m.F)
                    R=' '.join([str(f) for f in list(R.flatten())])+'\n'
                    T=' '.join([str(f) for f in list(T.flatten())])+'\n'
                    fo.write(F)
                    fo.write(R)
                    fo.write(T)
                    fo.write('{}\n'.format(m.n()))
                    fo.write(I.imageName+'\n')
                    fo.write(J.imageName+'\n')
    return
                
