#!/usr/bin/env python
#coding: utf8 
import numpy as np
import cv2
from zisserman import vgg_P_from_F,vgg_RT_from_F

def skew(v):
        """
        compute skew symmetric matrix
        thanks to http://pythonpath.wordpress.com/2012/09/04/skew-with-numpy-operations/
        """
        if len(v) == 4: v = v[:3]/v[3]
        skv = np.roll(np.roll(np.diag(v.flatten()), 1, 1), -1, 0)
        return skv - skv.T

def foo(fp1, q1, fp2,q2):
    """ 
        find the segment of minimum distance between two optical axes
        fp: focal point
        q: versor of the optical axis
        l>0
        optical axes: fp+l*q
        
        q3= versor(q1 x q2)
        q3 orthogonal to q1 and q2 , is the direction of the minimum distance segment
        |q1|=|q2|=|q3|=1        
        
        looking at vectors, solve for [l1,l2,d]:
            fp2+l2*q2+ q3*d = p1+l1*q1
            fp2+l2*q2+ q3*d - p1 -l1*q1 =0
            -l1*q1 + l2*q2 + q3*d = fp2-fp1
            [-q1 | q2 | q3 ] * [l1,l2,d] = (fp2-fp1)
        Ax=B
        x=[l1,l2,d]
        A=[-q1 | q2 | q3 ]
        B=(fp2-fp1)
    """
    q1=q1/np.linalg.norm(q1)
    q2=q2/np.linalg.norm(q2)
    q3=np.cross(q1,q2)
    angle=np.arcsin(np.linalg.norm(q3))
    q3=q3/np.linalg.norm(q3)
    B=fp2-fp1
    A=np.column_stack(-q1,q2,q3)
    x=np.linalg.solve(A,B)
    if not (x[0]>0 and x[1]>0 ) :
        return None
    scale_coherence=np.absolute(np.log(x[0]/x[1]))
    separation_coefficient=(x[2]*x[2])/(x[1]*x[0])
    baseline=np.linalg.norm(B)
    return (scale_coherence,separation_coefficient,baseline,angle)

def boo(fp1, q1, fp2,q2,firstStep=1,multiplier=2,relativeRadius=1/2,n=5):
    L=[(firstStep*multiplier^i) for i in range(n) ]
    P1=[fp1+l*q1 for l in L ]
    P2=[fp2+l*q2 for l in L ]
    D=[[np.linalg.norm(p1-p2) for p1 in P1] for p2 in P2]


"""
Nuova idea:
date due immagini si selezionano P= { punti} su un immagine e si proiettano sull' altra le E(P) {linee} epipolari di P.
Se E(P) interseca il campo immagine allora si ritorna un positivo , altrimenti un negativo.
I punti P sono indicativamente ai 4 angoli dell' immagine (0,0)(0,1)(1,1)(1,0).


=calcolo linea epipolare=
    l: linea in J , p: punto in I , F: matrice fondamentale I->J
    l= F p

=intersezione=
in coordinate omogenee p=l1 x l2 
usando prodotto vettoriale


X,Y= x/w , y/w
0<X<width
0<Y<height

0<x/w<width
0<y/w<height

0<x<width*w
0<y<height*w

x>0 && x<width*w && y>0 && y<height*w



=frame=
    frame(J) = [linee che delimitano l' immagine J]

immagini I, J
P=select()
L=[ np.mul(F,p) for p in P]
for l in L:
    intersections=0
    Lfj=frame_lines(J.width,J.height)
    for lj in Lfj:
        if validIntersection(l,lj,w,h) : intersections++
    if intersections==2 : 
        return True
    if intersections>=2 : 
        print ' più di due intersezioni... risultato inatteso'
        return True
return False





intrinsic=cv2.initCameraMatrix2D(3d_objectPoints, 2d_imagePoints, imageSize)

"""

  
        

def corner_points(w,h,margin=0.1):
    """return 4 points near the 4 corners of an image frame
    w = image width
    h = image height
    """
    assert(margin<1.)
    a=np.array([w*margin,h*margin,1.])
    b=np.array([w*(1.-margin),h*margin,1.])
    c=np.array([w*(1.-margin),h*(1-margin),1.])
    d=np.array([w*margin,h*(1.-margin),1.])
    return a,b,c,d

def edge_points(w,h,margin=0.1):
    """return 4 points near the middle points of the 4 edges of an image frame
    w = image width
    h = image height
    """
    assert(margin<1.)
    half_w=w/2.
    half_h=h/2.
    a=np.array([w*margin,half_h,1.])
    b=np.array([w*(1.-margin),half_h,1.])
    c=np.array([half_w,h*margin,1.])
    d=np.array([half_w,h*(1.-margin),1.])
    return a,b,c,d

def frame_lines(w,h,margin=0.1):
    """return the edges of the frame of an image"""
    a,b,c,d=corner_points(w,h,margin)
    ab=np.cross(a,b)
    bc=np.cross(b,c)
    cd=np.cross(c,d)
    da=np.cross(d,a)
    frame=[ab,bc,cd,da]
    frame=[l/l[2] for l in frame]
    return frame

def have_valid_intersection(l1,l2,w,h):
    """True if lines l1 and l2 have an intersection inside the frame"""
    hx=np.cross(l1,l2)
    return is_internal(hx,w,h)


def is_internal(hx,width,height):
    assert(len(hx)==3)
    if hx[2]==0 :
        return False
    x=hx[0]/hx[2]
    y=hx[1]/hx[2]
    return (x>=0) and (x<=width) and (y>=0) and (y<=height)

def have_valid_intersection(LI,LJ,w,h):
    for i in LJ:
        for j in LJ:
            if is_internal(np.cross(lj,li),w,h): return true
    return False

def epiline_filter(F,w,h,i_margin=0.1,j_margin=0.1):
    #corner points in image1
    P=corner_points(w,h,i_margin)
    #epilines in image 2 of corner points
    EpL=[ np.mul(F,p) for p in P]
    frame=frame_lines(w,h,j_margin)
    return have_valid_intersection(EpL,frame,w,h)

def F_from_poses(
        R_source,T_source,
        R_destination,T_destination,
        K_source,K_destination=None):
    #calculate rotation matrices from rodrigues angles
    Rmat_source=cv2.Rodrigues(R_source)[0]
    Rmat_destination=cv2.Rodrigues(R_destination)[0]
    K_s=K_source
    K_d=K_destination if type(K_destination)==np.ndarray else K_s
    Rmat_sd=np.dot(Rmat_source.T,Rmat_destination)
    T_sd=np.dot(Rmat_source.T, (T_destination-T_source))
    #rotation Rmat_sd and translation T_sd describe the rototranslation from source to destination
    #have a look 
    #@ Multiple View Geometry in Compurer Vision
    #A4.3 page 582
    #here we are calculating 9.4 at page 244
    #f=K'- T RK T [e] x
    skew_mat=skew(np.dot(
        np.dot(K_s,Rmat_sd.T),
        T_sd))
    b=np.dot(K_s,skew_mat)
    a=np.dot(
        np.linalg.inv(K_d.T),
        Rmat_sd)
    F=np.dot(a,b)
    F=F/F[2,2]
    return F  
    

