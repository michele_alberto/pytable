
import os


class paths:
    featureDir = "data/feat/"
    matchDir = "data/match/"
    imageDir = "data/images/"
    indexDir = "data/index/"
    pointDir = "data/points/"
    poseObservationDir="data/poseObservations/"
    @staticmethod
    def assertDir(dir):
        d = os.path.dirname(dir)
        assert(os.path.exists(d))
    @staticmethod
    def ensureDir(dir):
        d = os.path.dirname(dir)
        if not os.path.exists(d):
            os.makedirs(d)
    @staticmethod
    def ensureFile(f):
        ensureDir(f)
        if not os.path.isfile(f):
            open(f, 'a').close()

