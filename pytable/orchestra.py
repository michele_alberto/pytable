#!/usr/bin/env python
#coding: utf8 
#%pylab
import numpy as np
import cv2
import table as TABLE
import matchFilter as mf
import glob
from IPython.parallel import Client
from subprocess import call
from graph_tool.all import *
import matchEngine
from IPython.parallel import require
import transclosure
detectorSize=8000
detector_label='ORB{}'.format(detectorSize)

tab=TABLE.MatchTable()
rc=Client()
lbv=rc.load_balanced_view()


#----------DEFINIZIONE GRAFO----------------
G=Graph(directed=False) #the undirected graph
IB=G.new_vertex_property('int32_t') #vertex-> image index
FMap=G.new_edge_property("object")   
Association=G.new_edge_property('double') 
Observation = G.new_edge_property("bool")
Connectivity= G.new_vertex_property('double') 
                    
#----------MATCHING---------------- 
@require('pytable.matchEngine')  
def matchProcess_remote(request):
    import pytable.matchEngine
    Idump,Jdump,detectorLabel=request
    K=4
    photometric_tollerance=40
    ransac_tollerance=2.
    label='{}k{}p{}r{}'.format(detectorLabel,K,photometric_tollerance,ransac_tollerance)
    me=matchEngine.MatchEngine(Idump,Jdump,detectorLabel)
    matches=me.bestKMutual(K)
    matches1=me.photometric(matches,photometric_tollerance)
    if len(matches1)<20: return me.ij
    matches1,F=me.ransac(matches1,ransac_tollerance)
    if len(matches1)<12: return me.ij
    o=me.write(label,matches1)
    #abc_name=me.write_abc('profile',matches,F)
    #o['profile']=abc_name
    ans=(me.ij,label,o,F,len(matches1))
    return ans
    
def matchProcess_callback(ans):
    if len(ans)==5:
        ij,label,o,F,n=ans
        i,j=ij
        I=tab.getImage(i)
        J=tab.getImage(j)
        M=tab.getMatch(i,j)
        M.F=F
        M.matches[label]=o
        e=G.add_edge(i, j)
        M.edge=e
        M.nodes=(I.node,J.node)
        associationGain=float(n)/detectorSize
        Association[e]=associationGain
        Observation[e]=1
        for v in M.nodes: 
            Connectivity[v]=Connectivity[v]+associationGain
        FMap[e]=F
        return
    elif len(ans)==2:
        i,j=ans
        M=tab.getMatch(i,j)
        I=tab.getImage(i)
        J=tab.getImage(j)
        e=G.add_edge(i, j)
        M.edge=e
        M.nodes=(I.node,J.node)
        Observation[e]=1
        Association[e]=0.
        return

def match_orchestra(mbins,detectorlabel,delegate=True):
    n=len(mbins)
    requests=[(tab.getImage(m.Ii).dump(),tab.getImage(m.Ij).dump(),detectorlabel) for m in mbins]
    print 'sending match'
    if delegate:
        answers=lbv.map(matchProcess_remote,requests)
    else:
        answers=map(matchProcess_remote,requests)
    print 'receive match'
    for partial,ans in enumerate(answers):
        print partial,'/',n
        matchProcess_callback(ans)

#----------DETECT----------------
def detectProcess_remote(request):
    I,label,n,callback_id=request
    detector=cv2.ORB(n,nlevels=32)
    I.detect_and_train(detector,label)
    return (callback_id,I.kp[label])

def detectProcess_callback(I,label,ans):
    I.kp[label]=ans
    v=G.add_vertex()
    IB[v]=I.index
    I.node=v
    Connectivity[v]=0.

def detect_orchestra(ibins,detectorlabel):
    n=len(ibins)
    requests = [(ibin,detectorlabel,detectorSize,callback_id) for callback_id,ibin in enumerate(ibins)]
    Rmap={req[3]:req[0] for req in requests}
    print 'sending detect'
    answers=lbv.map(detectProcess_remote,requests)
    print 'receive detect'
    for partial,ans in enumerate(answers):
        Ibin=Rmap[ans[0]]
        kp_line=ans[1]
        print partial,'/',n
        detectProcess_callback(Ibin,detectorlabel,kp_line)
#----------CLUSTERING----------------
def mcl_view_clustering(matchBins,matchLabel):
    ABC=[]
    for M in matchBins:
        a=M.Ii
        b=M.Ij
        c=M.matches[matchLabel]['n']
        abc=(a,b,c)
        ABC.append(abc)
    f=open('temporaneo_view_graph.abc','w')
    for a,b,c in ABC:
        s='{} {} {}\n'.format(a,b,c)
        f.write(s)

def G2mcl(Graph,inEMap,filename,I,minCluster=0.05,maxCluster=0.6):
    with open(filename) as f:
        for e in Graph.edges():
            a=Graph.vertex_index[e.source()]
            b=Graph.vertex_index[e.target()]
            c=inEMap[e]
            s='{} {} {}\n'.format(a,b,c)
            f.write(s)
    inf=filename
    outf='tempOut.txt'
    s="mcl {} --abc -I {} -o {}".format(inf,I,outf)
    call(s)
    with  open(outf) as fo:
        ll=fo.readLines()
        clusters=[[int(s) for s in l.split('\t') ] for l in ll]
    if len(clusters)<minCluster*Graph.num_vertices:
        return None
    if len(clusters)>maxCluster*Graph.num_vertices:
        return None
    outVMap = Graph.new_vertex_property("int")
    for c,C in enumerate(clusters):
        for i in C:
            v=Graph.vertex(i)
            outVMap[v]=c
    return outVMap

def G2mcl_remote(inf,I,outf):
    #inf= input file
    #outf= ouput file
    #I=mcl inflation parameter
    s="mcl {} --abc -I {} -o {}".format(inf,I,outf)
    call(s,shell=True)
    with open(outf) as fo:
        ll=fo.readLines()
        clusters=[[int(s) for s in l.split('\t') ] for l in ll]
    if len(clusters)<minCluster*Graph.num_vertices:
        return None
    if len(clusters)>maxCluster*Graph.num_vertices:
        return None
    return clusters

def G2mcl_orchestra(Graph,inEMap,filename,Is,minCluster=0.05,maxCluster=0.6):
    with open(filename) as f:
        for e in Graph.edges():
            a=Graph.vertex_index[e.source()]
            b=Graph.vertex_index[e.target()]
            c=inEMap[e]
            s='{} {} {}\n'.format(a,b,c)
            f.write(s)
    tuples=[(filename,i,'tempOut{}.txt'.format(j)) for j,i in enumerate(Is)]
    #clusterSpace is the space of plausible clustering models for a given graph
    #it is the sampling of a random solution = clustering( deterministic set random Inflation)
    clusterSpace=map(G2mcl_remote,tuples)
    clusterSpace=[s for s in clusterSpace if s]
    elements=len(clusterSpace)
    clusterTableau=np.zeros((Graph.num_vertices,elements),np.int)
    for csi,cs in enumerate(clusterSpace):
        for ci,cluster in enumerate(cs):
            for v in cluster:
                clusterTableau[v,csi]=ci
    return clusterTableau

def sameCluster(i,j,clusterTableau):
    diff=clusterTableau[i,:]-clusterTableau[j,:]
    return sum([1 for d in diff if d==0])
    

def match_zero(n_imges,k):
    n=n_imges
    r=n-k
    matches_rr=[(i,j)  for i in range(r) for j in range(i,i+k) ]
    matches_kk=[(i,j)  for i in range(r,n) for j in range(i,n)]
    return matches_rr+matches_kk

def match_uno(n_imges,k1,k2):
    n=n_imges
    r=n-(k1+k2)
    matches_rr=[(i,j)  for i in range(0,r) for j in range(i+k1,i+k2+1) ]
    matches_kk=[(i,j)  for i in range(r,n) for j in range(i+1,n)]
    return matches_rr+matches_kk

def match_full(n_imges):
    n=n_imges
    matches_kk=[(i,j)  for i in range(n) for j in range(i,n)]
    return matches_kk

def EG_line(i,j,R,T):
    return ''.join(['{} {} '.format(self.i, self.j),
           '{} {} {} '.format(self.R[0,0], self.R[0,1], self.R[0,2]),
           '{} {} {} '.format(self.R[1,0], self.R[1,1], self.R[1,2]),
           '{} {} {} '.format(self.R[2,0], self.R[2,1], self.R[2,2]),
           '{} {} {}\n'.format(self.t[0], self.t[1], self.t[2])])


def G2img(G,Ass,Obs):
    arr=np.zeros((G.num_vertices(),G.num_vertices()),np.float)
    for e in G.edges():
        if Obs[e]:
            arr[int(e.source()),int(e.target())]=Ass[e]
            arr[int(e.target()),int(e.source())]=Ass[e]
    return arr

def G2EGfile(G,Ass,Obs,FMap,filename):
    with file(filename,'w') as output:
        for e in G.edges():
            if Obs[e]>0 and Ass[e]>0:
                i=int(e.source())
                j=int(e.target())
                Fij=FMap[e]
                Rij,Tij=a.vgg_RT_from_F(F)
                output.write(EG_line(i,j,Rijij,T))
    return filename

def visualizeAllMatches(mbins):
    V=[]
    for m in mbins:
        matchLabel='ORB4000k4p40r2.0'
        matches=m.mread(matchLabel)
        image=m.visualize_match(matches,detector_label)
        V.append(image)
    return

    
        
    
    



