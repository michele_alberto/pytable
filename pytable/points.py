import pytable.transclosure as tr
from settings import paths

def closure(mbinsList,ibinsList):
    pointTracks=tr.transitiveclosure()
    for m in mbinsList:
        matches=m.mread()
        if matches is not None:
            im_i=m.Ii
            im_j=m.Ij   
            TrackM=tr.transitiveclosure.fromMatchList(matches,im_i,im_j)
            pointTracks.closure(TrackM)
    for I in ibinsList:
        kp=I.kpread()
        pointTracks.add_view_definition(I.index,kp)
    return pointTracks

def export(pointTracks):
    path=paths.pointDir
    paths.ensureDir(path)
    for p_id in pointTracks.points:
        fileName='{}.txt'.format(p_id)
        fullName=path+fileName
        point=pointTracks.points[p_id]
        with file(fullName,'w') as f:
            for image_id in point.views:
                for v in point.views[image_id]:
                    s="{} {} {} {}\n".format(image_id,v.id_keypoint,v.v,v.u)
                    f.write(s)

def export(pointTracks,filenameOfPoint=None):
    if filenameOfPoint==None:
        callback=lambda p : p.id
    else :
        callback=filenameOfPoint
    path=paths.pointDir
    paths.ensureDir(path)
    for p_id in pointTracks.points:
        point=pointTracks.points[p_id]
        fileName='{}.txt'.format(callback(point))
        fullName=path+fileName
        with file(fullName,'w') as f:
            for image_id in point.views:
                for v in point.views[image_id]:
                    s="{} {} {} {}\n".format(image_id,v.id_keypoint,v.v,v.u)
                    f.write(s)





