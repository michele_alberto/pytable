#!/usr/bin/env python
#coding: utf8 
from settings import paths
import cv2
import numpy as np
from imageBin import ImageBin
from matchBin import MatchBin
from pylab import *
import video.v2kf as video

class MatchTable(object):
    def __init__(self,imageVector=[]):
        self._iv = imageVector
        self._mt = {}
        for i,I in enumerate(self._iv):
            for j,J in enumerate(self._iv):
                if j<i and J and I and J is not I:
                    assert(J.index==j)
                    assert(I.index==i)
                    IJ=MatchBin(self, I, J)
                    self._mt[I.index,J.index]=IJ

    def getImage(self, i):
        return self._iv[i]
    
    def allImages(self):
        return self._iv

    def getMatch(self, Ii, Ij):
        assert(Ii<len(self._iv) and Ij<len(self._iv))
        if Ii > Ij:
            try:
                return self._mt[Ii, Ij]
            except KeyError:
                I=self._iv[Ii]
                J=self._iv[Ij]
                IJ=MatchBin(self, I, J)
                self._mt[I.index,J.index]=IJ
                return IJ     
        else:
            try:
                return self._mt[Ij, Ii]
            except KeyError:
                I=self._iv[Ii]
                J=self._iv[Ij]
                JI=MatchBin(self, J,I)
                self._mt[J.index,I.index]=JI
                return JI 

    def allMatches(self):
        return [self._mt[m] for m in self._mt]

    def addImage(self, fileName, geometry=None):
        fullpath = paths.imageDir + fileName
        im = cv2.imread((fullpath), 0)
        if im is None:
            raise IOError(fullpath + " can not be opened as an image")
        n = len(self._iv)
        I = ImageBin(fileName, n, geometry)
        self._iv.append( I)
        for J in self._iv:
            if J and J is not I :
                IJ=MatchBin(self, I, J)
                self._mt[n,J.index]=IJ

    def addVideo(self,fileName,pictureBase='video'):
		OO=v2kf.video2keyframe(fileName)
		for i,im in enumerate(OO.tagAndSelect()):
			name=pictureBase+"{}.png".format(i)
			fullpath = paths.imageDir+name
			cv2.imwrite(fullpath,im)
			self.addImage(name)
	
    @staticmethod
    def load(data):
        tab=MatchTable()
        ivDump=data["views"]
        mtDump=data["matches"]
        imageList=[ImageBin.load(i) for i in ivDump]
        imageIndexList=[Im.index for Im in imageList]
        n_images=max(imageIndexList)+1
        print 'n immagini :',n_images
        imageVector=[None for i in range(n_images)]
        print imageVector
        for Im in imageList:
            print Im.index,Im.kp
            imageVector[Im.index]=Im
        print imageVector
        tab=MatchTable(imageVector)
        matchBins=[MatchBin.load(tab,m) for m in mtDump]
        for m in matchBins:
            tab._mt[m.Ii,m.Ij]=m
            print m.Ii,m.Ij,m.matches
        print tab.allImages()
        print tab.allMatches()
        return tab

    def dump(self):
        ivDump = [i.dump() for i in self.allImages()]
        mtDump = [mb.dump() for mb in self.allMatches()]
        return {"views":ivDump, "matches" : mtDump}
    
    def qualityInspection(self,match_label,detector_label):
        Q1,Q2=[],[]
        for M in self.allMatches():
            matches=M.mread(match_label)
            q1,q2=M.qualityInspection(matches,detector_label,M.F)
            Q1.append(q1)
            Q2.append(q2)
        Q1=np.hstack(tuple(Q1))
        Q2=np.hstack(tuple(Q2))
        return (Q1,Q2)
    
    def visQuality(self,match_label,detector_label):
        Q1,Q2=self.qualityInspection(match_label,detector_label)
        xlabel('photometric error')
        ylabel('geometric error')
        hist2d(Q1,Q2,bins =200)
        show()

        
        

