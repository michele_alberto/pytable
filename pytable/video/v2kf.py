import numpy as np
import cv2


class tagTree:
	def __init__(self):
		self.tags=[]
		self.tree={}
		self.tagCount=0;
	def newTag(self):
		t=self.tagCount
		self.tags.append(t)
		self.tree[t]=[]
		self.tagCount=self.tagCount+1
		return t

def fittness(img,npoints=100):
	kps=cv2.ORB(npoints).detect(img)
	s=sum([kp.response for kp in kps])
	return float(s)/npoints

def Low(img):
	low=cv2.pyrDown(img)
	return low


def convolutionMatch(im1,im2):
	quad_err=np.square(im1-im2)
	return np.mean(quad_err)

class video2keyframe:
	def __init__(self,filename,r=10,tollerance=15000.,increment=1.02):
		self.increment=increment
		self.tollerance=tollerance
		self.filename=filename

	def init_run(self):
		self.vc=cv2.VideoCapture(self.filename)
		self.counter=0
		
	def pop(self):
		res,im=self.vc.read()
		if not res:
			return -1,None
		n=self.counter
		self.counter+=1
		return n,im
	
	def seek(self,frameList):
		frameList.sort()
		self.init_run()
		ans=[]
		for seeking in frameList:
			c=True
			while c:
				fId,im=self.pop()
				if fId==-1 :
					c=False
				elif fId==seeking :
					o=(fId,im)
					ans.append(o)
					c=False
		return ans
	
	def same(self,img1,img2):
		squared=np.square(img1.astype(float)-img2.astype(float)	)
		score= np.mean(squared)
		return score/self.tollerance

	def tag(self):
		tt=tagTree()
		self.init_run()
		c=True
		tag=None
		kp=None
		kp_low=None
		sign=[]
		while c:
			n,im=self.pop()
			if n==-1:
				return tt,sign
			if n==0:
				#first iteration
				tag=tt.newTag()
				kp=im
				kp_low=Low(im)
				o=(n,fittness(im))
				tt.tree[tag].append(o)
				coefficient=1.
			else:
				#loop iteration
				im_low=Low(im)
				matching=self.same(im_low,kp_low)
				normalized=matching*coefficient
				sign.append(normalized)
				o=(n,fittness(im))
				if normalized<1:
					#matchcase
					tt.tree[tag].append(o)
					coefficient=coefficient*self.increment
				else:
					#non-matching case
					#print 'new tag'
					tag=tt.newTag()
					kp=im
					kp_low=im_low
					coefficient=1.
					tt.tree[tag].append(o)
		return tt,sign
	
	def tagAndSelect(self):
		tt=self.tag()
		best_frames=[]
		for t in tt.tags:
			framelist,fittness=zip(*tt.tree[t])
			i=np.argmax(fittness)
			keyframe=framelist[i]
			best_frames.append(keyframe)
		return best_frames
		
			

