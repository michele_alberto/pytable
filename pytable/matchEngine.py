import numpy as np
import cv2
from imageBin import ImageBin
import matchFilter as mf
from settings import paths

class MatchEngine(object):
    def __init__(self,Idump,Jdump,detectorLabelI,detectorLabelJ=None):
        self.detectorLabelI=detectorLabelI
        self.detectorLabelJ=(detectorLabelJ if detectorLabelJ else detectorLabelI)
        self.imI=ImageBin.load(Idump)
        self.imJ=ImageBin.load(Jdump)
        self.Ii=self.imI.index
        self.Ij=self.imJ.index
        self.ij=(self.Ii,self.Ij)
        self.kpI=self.imI.kpread(self.detectorLabelI)
        self.kpJ=self.imJ.kpread(self.detectorLabelJ)	
        self.ftI=self.imI.ftread(self.detectorLabelI)
        self.ftJ=self.imJ.ftread(self.detectorLabelJ)
    def ransac(self,matches,e,confidence=0.99):
        out_matches,F=mf.filter_matches_ransac_indexed(matches,self.kpI,self.kpJ,e,confidence)
        return out_matches,F
    def photometric(self,matches,tollerance):
		out_matches=mf.filter_matches_photometric_indexed(matches,self.ftI,self.ftJ,tollerance)
		return out_matches
    def geometric(self,matches,F,tollerance):
        out_matches=mf.filter_matches_geometric_indexed(matches,F,tollerance,self.kpI,self.kpJ)
        return out_matches
    def bestKMutual(self,K):
        J2I=self.imI.match_feature_to(self.ftJ,self.detectorLabelI,K=K,featureDataset=self.ftI)
        I2J=self.imJ.match_feature_to(self.ftI,self.detectorLabelJ,K=K,featureDataset=self.ftJ)
        return mf.k_mutual_best_matches(I2J,J2I)
    def bestKDual(self,K):
        J2I=self.imI.match_feature_to(self.ftJ,self.detectorLabelI,K=K,featureDataset=self.ftI)
        I2J=self.imJ.match_feature_to(self.ftI,self.detectorLabelJ,K=K,featureDataset=self.ftJ)
        return mf.k_dual_best_matches(I2J,J2I)
    def write(self, matchlabel, matches):
        if len(matches)>0:
            match_array=np.array(matches)
            path=paths.matchDir + matchlabel + '/'
            fileName="match{}x{}.npz".format(self.Ii,self.Ij)
            fullName=path+fileName
            paths.ensureDir(path)
            np.savez(fullName, match_array=match_array)
            o={'matchFile':fullName,'n':len(match_array)}
        else:
            o={'n':0}
        return o
    def geometricError(self,matches,F):
        return mf.geometric_error(matches,F,self.kpI,self.kpJ)
    def photometricError(self,matches):
        return mf.photometric_error(matches,self.ftI,self.ftJ)
    def write_abc(self,abc_label,matches,F,bank_size=4000):
        Geom=self.geometricError(matches,F)
        Phot=self.photometricError(matches)
        path=paths.matchDir + matchlabel + '/'
        fileName="{}{}x{}.abc".format(abc_label,self.Ii,self.Ij)
        fullName=path+fileName
        paths.ensureDir(path)
        baseI=bank_size*self.Ii
        baseJ=bank_size*self.Ij
        baseC=0.0000001
        with open(fullName,'w') as f:
            for mi,m in enumerate(matches):
                a=baseI+m[0]
                b=baseJ+m[1]
                c=(Geom[mi]/2.)+(Phot[mi]/30.)+baseC
                c=1/c
                s="{} {} {}\n".format(a,b,c)
                f.write(s)
        return filename
        

    
