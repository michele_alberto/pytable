import cv2
import numpy as np
from hamming import hamming

def k_best_matches(I2J,maxJ,direct=True):
    matches=[]
    if direct:
        for indexI,neighbourI in enumerate(I2J):
            for indexJ in neighbourI:
                if (indexJ<maxJ) :
                    matches.append((indexI,indexJ))
    else:
        for indexI,neighbourI in enumerate(I2J):
            for indexJ in neighbourI:
                if (indexJ<maxJ) :
                    matches.append((indexJ,indexI))
    return matches

def k_dual_best_matches(I2J,J2I):
    maxJ=len(J2I)
    maxI=len(I2J)
    matchesI2J=k_best_matches(I2J,maxJ)
    matchesJ2I=k_best_matches(J2I,maxI)
    matchesI2J.extend(matchesJ2I)
    matches=uniqueMatches(matchesI2J)
    return matches

def k_mutual_best_matches(I2J,J2I):
    matches=[]
    maxJ=len(J2I)
    for indexI,neighbourOfI in enumerate(I2J):
        for indexJ in neighbourOfI:
            if (indexJ<maxJ) and (J2I[indexJ] == indexI).any() :
                matches.append((indexI,indexJ))
    return matches

def uniqueMatches(matches):
	mask_shit=16
	key=np.left_shift(MM[:,0],mask_shit)+MM[:,1]
	u_keys,u_index=np.unique(MMkey,return_index=True)
	return MM[u_index]

def filter_matches_photometric_indexed(matches,featI,featJ,e):
    columnI=np.array([featI[match[0] ] for match in matches])
    columnJ=np.array([featJ[match[1] ] for match in matches])
    dtype=featI.dtype
    assert(featI.dtype==featJ.dtype)
    if dtype==np.uint8:
        columnI.dtype=np.uint64
        columnJ.dtype=np.uint64
        distance=sum(hamming(columnI,columnJ).T)
    elif dtype in [np.float,np.float32,np.float64]:
        distance=sum(np.square((columnI-columnJ)).T)
    else:
        assert(dtype in [np.float,np.uint8])
    mask=distance<e
    filtered_matches=[]
    for it,m in enumerate(matches):
        if mask[it]:
            filtered_matches.append(m)
    return filtered_matches
    
def filter_matches_geometric_indexed(matches,F,e,PointsI,PointsJ):
    filtered_matches=[]
    assert(len(PointsI)>0)
    assert(len(PointsJ)>0)
    homogeneusI=cv2.convertPointsToHomogeneous(PointsI.astype(np.float32))
    homogeneusJ=cv2.convertPointsToHomogeneous(PointsJ.astype(np.float32))
    for it,m in enumerate(matches):
        hi=homogeneusI[m[0]]
        hj=homogeneusJ[m[1]]
        Ferr=np.dot(hi ,np.dot( F , hj.T ))
        if Ferr<e and Ferr>-e:
            filtered_matches.append(m)
    return filtered_matches

def filter_matches_ransac_indexed(matches,pointsI,pointsJ,e,confidence=0.99):
    assert(pointsI.dtype==np.float32)
    assert(pointsJ.dtype==np.float32)
    Pi=[]
    Pj=[]
    for m in matches:
        Pi.append(pointsI[m[0]])
        Pj.append(pointsJ[m[1]])
    Pi=np.array(Pi)
    Pj=np.array(Pj)
    F,mask=cv2.findFundamentalMat(Pi,Pj,cv2.FM_RANSAC,param1=e,param2=confidence)
    resultMatchVector=[]
    for it in range(len(mask)):
        if mask[it]==1:
            resultMatchVector.append(matches[it])
    return resultMatchVector,F

def explore_match(matches,kp1,kp2,img1,img2):
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]
    vis = np.zeros((max(h1, h2), w1+w2), np.uint8)
    vis[:h1, :w1] = img1
    vis[:h2, w1:w1+w2] = img2
    vis = cv2.cvtColor(vis, cv2.COLOR_GRAY2BGR)
    p1l=[]
    p2l=[]
    for i,j in matches:
        p1l.append(kp1[i])
        p2l.append(kp2[j])
    p1 = np.int32(p1l)
    p2 = np.int32(p2l) + (w1, 0)
    green = (0, 255, 0)
    red = (0, 0, 255)
    white = (255, 255, 255)
    kp_color = (51, 103, 236)
    for (x1, y1), (x2, y2) in zip(p1, p2):
        col = green
        cv2.circle(vis, (x1, y1), 2, col, -1)
        cv2.circle(vis, (x2, y2), 2, col, -1)
    for (x1, y1), (x2, y2) in zip(p1, p2):
        cv2.line(vis, (x1, y1), (x2, y2), green)
    return vis

def geometric_error(matches,F,PointsI,PointsJ):
    homogeneusI=cv2.convertPointsToHomogeneous(PointsI.astype(np.float32))
    homogeneusJ=cv2.convertPointsToHomogeneous(PointsJ.astype(np.float32))
    E=[]
    for it,m in enumerate(matches):
        hi=homogeneusI[m[0]]
        hj=homogeneusJ[m[1]]
        Ferr=np.dot(hi ,np.dot( F , hj.T ))
        E.append(np.absolute(Ferr))
    return E


def photometric_error(matches,featI,featJ):
    columnI=np.array([featI[match[0] ] for match in matches])
    columnJ=np.array([featJ[match[1] ] for match in matches])
    dtype=featI.dtype
    assert(featI.dtype==featJ.dtype)
    if dtype==np.uint8:
        columnI.dtype=np.uint64
        columnJ.dtype=np.uint64
        distance=sum(hamming(columnI,columnJ).T)
    elif dtype in [np.float,np.float32,np.float64]:
        distance=sum(np.square((columnI-columnJ)).T)
    else:
        assert(dtype in [np.float,np.uint8])
    return distance
        


