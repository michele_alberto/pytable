from settings import paths
import cv2
import asift.asift as asift
import numpy as np
import weakref as wr
import pykgraph as kg

def kp2tuple(kp):
    return (
        kp.angle,
        kp.octave,
        kp.pt,
        kp.response,
        kp.size)
def kp2np(kp):
    return np.array([
        kp.angle,
        kp.octave,
        kp.pt[0],kp.pt[1],
        kp.response,
        kp.size])
def tuple2kp(t):
    return cv2.KeyPoint(
        t[2][0],t[2][1],
        t[4],
        t[0],
        t[3],
        t[1])

def K_parameters(focal_lenght,mx=1.,my=1.,u=0.,v=0.,skew=0.):
    return np.array([
                    focal_lenght*mx, #0
                    focal_lenght*my, #1
                    u,               #2
                    v,               #3
                    skew])           #4
def K_from_parameters(i):
    K=np.zeros((3,3)).astype(np.double)
    K[0,0]=i[0]
    K[1,1]=i[1]
    K[0,1]=i[4]
    K[0,2]=i[2]
    k[1,2]=I[3]
    return K


def train_float(dataset,iterations=100, L=50, K=10, S=10, controls=100, delta=0.005, recall=0.9, prune=1):
    index=kg.KGraph()
    index.build(dataset,iterations, L, K, S, controls, delta, recall, prune)
    return index

def train_uint(dataset,iterations=20, L=64, K=8, S=30, controls=100, delta=0.005, recall=0.95, prune=1):
    index=kg.KGraph()
    index.build(dataset,iterations, L, K, S, controls, delta, recall, prune)
    return index

class ImageBin(object):
    def __init__(self, imageName, index, geometry=None ):
        """
        imageName: name of the image file
        index: imageBin index in the table
        geometry : (k,r,t)
            k : intrinsic matrix 
            r : rodrigues angles
            t : translation vector    
        """
        self.index = index
        self.imageName = imageName
        self.geometry = geometry
        self.kp = {}
        self._wrCache={}
        self.node=None

    def dump(self):
        model = {}
        model["index"] = self.index
        model["name"] = self.imageName
        if self.geometry:
            model["geometry"] = self.geometry
        if not self.kp == {}:
            model["kp"] = self.kp
        return model
    
    @staticmethod
    def load(o):
        i=o["index"]
        name = o["name"]
        if "geometry" in o:
            geometry = o["geometry"]
        else:
            geometry = None
        ib=ImageBin(name,i,geometry)
        if "kp" in o:
            ib.kp = o["kp"]
        return ib

    def index2file(self,index,label):
        paths.ensureDir(paths.indexDir + self.imageName + '/')
        fileName=paths.indexDir + self.imageName + '/' + label
        index.save(fileName)
        return fileName
    
    def imageFile(self):
        return paths.imageDir + self.imageName
    
    def imread(self):
        return cv2.imread(self.imageFile(), 0)

    def kpftwrite(self,label,keypoints,features):
        assert(not self.kp.has_key(label))
        assert(type(keypoints)==np.ndarray)
        assert(keypoints.dtype==np.float32)
        assert(type(features)==np.ndarray)
        paths.ensureDir(paths.featureDir + self.imageName + '/')
        fileName=paths.featureDir + self.imageName + '/' + label+'.npz'
        data=np.array( [keypoints,features])
        np.savez(fileName, keypoints=keypoints, features=features)
        self.kp[label] = {'archive':fileName}
        self._wrCache[label]={'keypoints':wr.ref(keypoints),'features':wr.ref(features)}
        return fileName
    def _infer_label(self,in_label=None):
        if in_label is not None:
            return in_label
        elif len(self.kp)>0:
            return list(self.kp)[0]
        else :
            return None
    def ftread(self,detector_label=None):
        label=self._infer_label(detector_label)
        assert(self.kp.has_key(label))
        if self._wrCache.has_key(label) and self._wrCache[label].has_key('features') and self._wrCache[label]['features']() is not None :
            data = self._wrCache[label]['features']()
            return data            
        npzfile = np.load(self.kp[label]['archive'])
        data=npzfile['features']
        if not self._wrCache.has_key(label):
            self._wrCache[label]={}
        self._wrCache[label]['features']=wr.ref(data)
        return data
    
    def kpread(self,detector_label=None):
        label=self._infer_label(detector_label)
        assert(self.kp.has_key(label))
        if self._wrCache.has_key(label) and self._wrCache[label].has_key('keypoints') and  self._wrCache[label]['keypoints']() is not None :
            data = self._wrCache[label]['keypoints']()
            return data
        npzfile = np.load(self.kp[label]['archive'])
        data=npzfile['keypoints']
        if not self._wrCache.has_key(label):
            self._wrCache[label]={}
        self._wrCache[label]['keypoints']=wr.ref(data)
        return data

    def ixread(self,detector_label=None):
        label=self._infer_label(detector_label)
        assert(self.kp.has_key(label))
        assert(self.kp[label].has_key('index'))
        index=kg.KGraph()
        index.load(self.kp[label]['index'])
        return index

    def detect(self, detector,label, mask=None):
        kps_cv2, features = detector.detectAndCompute(self.imread(), mask)
        kps_np=np.array([np.array(kp.pt) for kp in kps_cv2]).astype(np.float32)
        fileName=self.kpftwrite(label,kps_np,features)
        self.kp[label] = {'archive':fileName}
        return (kps_np,features)

    def Adetect(self, detector,label, mask=None, pool=None):
        kps_cv2, features = asift.affine_detect(detector, self.imread(), mask,pool=pool)
        #kps_cv2, features = detector.detectAndCompute(self.imread(), mask)
        kps_np=np.array([np.array(kp.pt) for kp in kps_cv2]).astype(np.float32)
        fileName=self.kpftwrite(label,kps_np,features)
        self.kp[label] = {'archive':fileName}
        return (kps_np,features)

    def train_index(self,label,dataset=None):
        assert(self.kp.has_key(label))
        if dataset==None:
            dts=self.ftread(label)[1]
        else:
            dts=dataset
        assert(type(dts)==np.ndarray)
        case=dts.dtype
        if case==np.uint8:
            index=train_uint(dts)
        elif case==np.float32:
            index=train_float(dts)
        else:
            return None
        fileName=self.index2file(index,label)
        self.kp[label]['index']=fileName
        return index

    def detect_and_train(self, detector,label, mask=None):
        kps_np,features = self.detect(detector,label, mask)
        index=self.train_index(label,features)
        return index
    
    def Adetect_and_train(self, detector,label, mask=None, pool=None):
        kps_np,features = self.Adetect(detector,label, mask,pool=pool)
        index=self.train_index(label,features)
        return index

    def match_feature_to(self,queryFeatureSet,detector_label=None,K=2,featureIndex=None,featureDataset=None):
        """
        perform K-Nearest N matching 
        queryFeatureSet : a numpy array of features to be matched
        [detector_label]: the label of the descriptor , the first descriptor calculated if not specified
        [K = 2]         : the number of best matches to calculate for each query feature
        [featureIndex]  : optional index 
        [featureDataset]: optional dataset

        return a numpy 2-dimensional array with 
            one row for each feature
            K columns
        elements in the array are the indexes of the matching features in featureDataset
        featureIndex , featureDataset are optional if not provided they will be retrieved form disk
        """
        label=self._infer_label(detector_label)
        assert(self.kp.has_key(label))
        assert(self.kp[label].has_key('archive') )
        assert(self.kp[label].has_key('index') )
        index=featureIndex if featureIndex is not None else self.ixread(label)
        if type(featureDataset) is np.ndarray  :
            dts= featureDataset
        else:
            dts=self.ftread(label)
        matchTable=index.search(dts,queryFeatureSet,K)
        return matchTable
        

