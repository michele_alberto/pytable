#!/usr/bin/env python
#coding: utf8 
from itertools import groupby,chain
#transitiveClosure è una facory di punti
class transitiveclosure(object):
    def __init__(self):
        self.points={} #mappa point_id->[point]
        self.max=0
        self.freePoints=[]
    def __str__(self):
        return 'transitive closure:\n'+'\n'.join([str(p) for p in self.points.values()])
    def _insertPoint(self,point):
        assert(point.id==0)
        if len(self.freePoints)>0:
            point.id=self.freePoints.pop()
        else :
            self.max+=1
            point.id=self.max
        self.points[point.id]=point
    def removePoint(self,point_id):
        del self.points[point_id]
        if point_id==self.max:
            self.max-=1
            while self.max in self.freePoints:
                self.max-=1
        else:
            self.freePoints.append(point_id)
        return
    def add_edge(self,v1,v2):
        new_point=self.newPoint([v1,v2])
        i1=v1.id_image
        i2=v2.id_image
        pointsIn1ToBeRemoved=[]
        for p_id in list(self.points):
            if point.have_intersection(self.points[p_id],new_point):
            #if v1 in self.points[p_id].views[i1] or v2 in self.points[p_id].views[i2]:
                new_point.mergeWith(self.points[p_id])
                pointsIn1ToBeRemoved.append(p_id)
        for p_id in pointsIn1ToBeRemoved:
            self.removePoint(p_id)
        return
    def newPoint(self,views):
        #v1 e v2 sono viste
        pointId=0 
        p=point(pointId,views)
        self._insertPoint(p)
        return p
    def closure(self,trc2):
        trc1=self
        for p2Id in trc2.points.keys(): #per ogni punto in trc2
            p2=trc2.points[p2Id]
            new_point=point(0,[]) #si crea un nuovo punto che sarà una copia di p2
            new_point.mergeWith(p2)
            pointsIn1ToBeRemoved=[] #punti corrispondenti a p2 in trc1 che verranno riscritti in newpoint
            for p1Id in trc1.points.keys():
                p1=trc1.points[p1Id]
                if point.have_intersection(p1,p2): #se vi è intersezione
                    new_point.mergeWith(p1) #new point "raccogie" le viste
                    pointsIn1ToBeRemoved.append(p1Id)
            for p1Id in pointsIn1ToBeRemoved:
                trc1.removePoint(p1Id)#remove merged points from trc1
            self._insertPoint(new_point) #new_point viene inserito in trc1
        return 
    
    @staticmethod
    def fromMatchList(matches,im_i,im_j):
        tr=transitiveclosure()
        for ei,e in enumerate(matches):
            i,j=e
            v1=view(im_i,i,-1,-1)
            v2=view(im_j,j,-1,-1) 
            tr.add_edge(v1,v2)
        return tr
    def add_view_definition(self,im_i,kp):
        for p_id in list(self.points):
            if self.points[p_id].views.has_key(im_i):
                for v in self.points[p_id].views[im_i]:
                    kp_id=v.id_keypoint
                    pointOnImage=kp[kp_id]
                    v.u=pointOnImage[0]
                    v.v=pointOnImage[1]
        return self
    def query_image_list(self,image_ids,VI,VIH):
        queryHeader=reduce(lambda h1,h2:h1|h2,[1<<(image_id%64) for image_id in image_ids])
        points=[]
        for p_id in self.points:
            p=self.points[p_id]
            if (p.viewHeader & queryHeader==queryHeader):
                points.append(p)
        return points
                
    def query_image_pair(self,image_id_i,image_id_j,VI,VIH):
        points=self.query_image_list([image_id_i,image_id_j],VI,VIH)
        kpI=[]
        kpJ=[]
        for p in points:
            for vi in p.views[image_id_i]:
                for vj in p.views[image_id_j]:
                    matches.append((vi.id_keypoint, vj.id_keypoint))
                    kpI.append([vi.u,vi.v])
                    kpJ.append([vj.u,vj.v])
        return matches,kpI,kpJ
    def F_speculation(self,image_id_i,image_id_j,ransac_population=0):
        matches,kpI,kpJ=self.query_image_pair(image_id_i,image_id_j)
        if ransac_population==0:
            if len(matches)<10:
                return None
            ptI=np.array(kpI)
            ptJ=np.array(kpJ)
        else :
            ptI=np.array(kpI[0:ransac_population])
            ptJ=np.array(kpJ[0:ransac_population])
        F,Mask=cv2.findFoundamentalMat(ptI,ptJ)
        return F
    def prune(self,minimum_visibility):
        newTR=transitiveclosure()
        k=minimum_visibility
        for p_id in self.points.keys():
            if len(self.points[p_id].views)>=k:
                newViews=reduce(lambda l1,l2:l1+l2,self.points[p_id].views.values(),[])
                np=newTR.newPoint(newViews)
        return newTR
    def prune_in_place(self,minimum_visibility):
        k=minimum_visibility
        for p_id in self.points.keys():
            if len(self.points[p_id].views)<k:
                self.removePoint(p_id)
        return newTR
    def viewIndex(self):
        VI={} #mappa im_id->[points]
        for p_id in self.points:
            p=self.points[p_id]
            for im_id in p.views:
                if VI.has_key(im_id):
                    VI[im_id].append(p_id)
                else:
                    VI[im_id]=[p_id]
        return VI
    def viewIndexHeader(self,VI,size=256):
        header={im_id:reduce(lambda h1,h2:h1|h2,[1<<(p_id%size)for p_id in VI[im_id] ]) for im_id in VI}
        return header
class point :
    def __init__(self,point_id,views,header=None):
        self.id=point_id
        self.views={id_image:list(group_of_views) for id_image, group_of_views in groupby(views, lambda v: v.id_image)}
        #mappa id_immagine -> vista
        if len(views)>0:
            self.header=header if header else reduce(lambda h1,h2: h1|h2,[v.header() for v in views])
        else:
            self.header=0L
        self.viewHeader=reduce(lambda h1,h2: h1|h2,[1<<(id_image%64) for id_image in self.views],0L)
    def __str__(self):
        header="point <id:{}\n".format(self.id)
        body='\n'.join(['\t'+str(v) for im_id in self.views for v in self.views[im_id]])
        return header+body+'>\n'
    @staticmethod
    def have_intersection(p1,p2):
        if (p1.header & p2.header)>0: #l' header può escludere un possibile match  
            for image_id in p1.views.keys(): #per ogni immagine che vede p1
                if p2.views.has_key(image_id): #se questa è vista anche da p2
                    for i in p1.views[image_id]: #si cerca un intersezione tra i due insiemi di osservazioni per quell' immagine
                        for j in p2.views[image_id]:
                            if i.id_keypoint==j.id_keypoint:
                                return True
        return False
    def getKeypointsIds(self,image_id):
        return [v.id_keypoint for v in self.views[image_id]]
    def allViews(self):
        return [v for im_id in self.views for v in self.views[im_id]]
    def mergeWith(self,otherP): #implementa una riscrittura di otherP come self
        self.header=self.header | otherP.header #merge headers
        self.viewHeader=self.viewHeader | otherP.viewHeader
        #now merge view sets without duplicates
        for image_id in otherP.views.keys():  #per ogni immagine che vede il punto importato
            if self.views.has_key(image_id):  #caso A) se questa è già contenuta nel punto orginale
                view_set_for_image_id= set(chain (otherP.views[image_id],self.views[image_id])) 
                #unione con vincolo di unicità, funziona grazie a view.__eq__
                self.views[image_id]=list(view_set_for_image_id)
            else :#caso B) se questa non è contenuta nel punto orginale
                self.views[image_id]=otherP.views[image_id] #il vettore di viste viene importato
        return self

class view (object):
    def __init__(self,id_image,id_keypoint,u,v):
        self.id_image=id_image
        self.id_keypoint=id_keypoint
        self.u=u
        self.v=v
    def __str__(self):
        return "view <im:{},kp:{},u:{},v:{}>".format(self.id_image,self.id_keypoint,self.u,self.v)
    def __hash__(self):
        return (self.id_image<<15)+self.id_keypoint
    def __eq__(self, other):
        if (self.id_image==other.id_image) and (self.id_keypoint==other.id_keypoint):
            return True
        else:
             return False
    def header(self):
        return 1<<((self.id_image+self.id_keypoint)%64)

"""
transitive closure 
CLUSTERS= 
    CLUSTER_iD
    header = OR[1 << (idFeature+idImage)%64]
    body: [( idFeature,idImage,x,y)],
    
call model:(matches,i
,j) 
F: (matches,i,j) -> CLUSTER
G: (CLUSTER,CLUSTER) -> CLUSTER

F:
for ie,e in enumerate(matches):
    CLUSTER_iD:=$counter

a=view(0,0,10.,20.)
b=view(0,1,10.,20.)
c=view(0,1,10.,20.)
d=view(1,1,10.,20.)
e=view(0,0,10.,20.)
sa=list(set([a,b,c,d,e]))

    header
    body: [(e.i,i,i.kp[e.i])],

"""
