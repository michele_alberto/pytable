import numpy as np
from hamming import hamming

def popcnt(i):
    v=i
    count=0
    while v!=0 :
        count=count+1
        v=v&(v-1)
    return count

def oracle(a,b):
    return popcnt(a^b)


def verify(dtsA,dtsB):
    res= hamming(dtsA,dtsB)
    for r,a,b in np.nditer([res,dtsA,dtsB]):
        assert(r==oracle(a,b))
    print 'ok:',dtsA.dtype

n,bin_bytes =200,64
d1 = np.random.randint(256,size=(n,bin_bytes)).astype(np.uint8)
d2 = np.random.randint(256,size=(n,bin_bytes)).astype(np.uint8)
verify(d1,d2)
d1.dtype=np.uint16
d2.dtype=np.uint16
verify(d1,d2)
d1.dtype=np.uint32
d2.dtype=np.uint32
verify(d1,d2)
d1.dtype=np.uint64
d2.dtype=np.uint64
verify(d1,d2)

