#include "Python.h"
#include "math.h"
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/halffloat.h"

/*
 * hamming.c
 * This is the C code for creating a
 * Numpy ufunc to calculate the hamming distance. 
 * To do so efficiently the a POPCOUNT gcc primitive is used.
 *
 * Details explaining the Python-C API can be found under
 * 'Extending and Embedding' and 'Python/C API' at
 * docs.python.org .
 * Michele Alberto
 */

typedef const unsigned long long chunk_t;

static PyMethodDef HammingMethods[] = {
        {NULL, NULL, 0, NULL}
};

/* The loop definition must precede the PyMODINIT_FUNC. */

static void ull_hamming(char **args, npy_intp *dimensions,
                            npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp n = dimensions[0];
    char *in1 = args[0], *in2 = args[1];
    char *out1 = args[2];
    npy_intp in1_step = steps[0], in2_step = steps[1];
    npy_intp out1_step = steps[2];


    for (i = 0; i < n; i++) {
        /*BEGIN main ufunc computation*/
        chunk_t c1 = *(chunk_t *)in1;
        chunk_t c2 = *(chunk_t *)in2;
        chunk_t cXor = c1 ^ c2;
        *((unsigned long long *)out1) = __builtin_popcountll(cXor);
        /*END main ufunc computation*/
        in1 += in1_step;
        in2 += in2_step;
        out1 += out1_step;
    }
}



/*This a pointer to the above function*/
PyUFuncGenericFunction funcs[1] = {&ull_hamming};

/* These are the input and return dtypes of logit.*/

static char types[3] = {NPY_UINT64, NPY_UINT64,
                        NPY_UINT64};


static void *data[1] = {NULL};

#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "hamming",
    NULL,
    -1,
    HammingMethods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit_hamming(void)
{
    PyObject *m, *hamm, *d;
    m = PyModule_Create(&moduledef);
    if (!m) {
        return NULL;
    }

    import_array();
    import_umath();

    hamm = PyUFunc_FromFuncAndData(funcs, data, types, 1, 2, 1,
                                    PyUFunc_None, "hamming",
                                    "calculate the hamming distance docstring", 0);

    d = PyModule_GetDict(m);

    PyDict_SetItemString(d, "hamming", hamm);
    Py_DECREF(hamm);

    return m;
}
#else
PyMODINIT_FUNC inithamming(void)
{
    PyObject *m, *hamm, *d;


    m = Py_InitModule("hamming", HammingMethods);
    if (m == NULL) {
        return;
    }

    import_array();
    import_umath();

    hamm = PyUFunc_FromFuncAndData(funcs, data, types, 1, 2, 1,
                                    PyUFunc_None, "hamming",
                                    "calculate the hamming distance docstring", 0);

    d = PyModule_GetDict(m);

    PyDict_SetItemString(d, "hamming", hamm);
    Py_DECREF(hamm);
}
#endif
