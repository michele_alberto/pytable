import numpy as np
import cv2
import table as TABLE
import matchFilter as mf
import settings
import glob
import itertools as it
from multiprocessing.pool import ThreadPool
import geometricFilter as gf
import parser_aermatica_telemetrie as p
from imageBin import ImageBin
from matchBin import MatchBin
tab=TABLE.MatchTable()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

def matchProcess(M,label,matchLabel,photo=30,rans=5.):
    if M.matches.has_key(matchLabel):
        return
    matches=M.match(label,4,photometric=40)
    if len(matches)<10:
        matches=[]
        M.mwrite(matchLabel,matches)
        print M.Ii,M.Ij," have no common points"
        return
    imI=tab.getImage(M.Ii)
    imJ=tab.getImage(M.Ij) 
    kpI=imI.kpread(label)
    kpJ=imJ.kpread(label)
    matchesLR,M.F=mf.filter_matches_ransac_indexed(matches,kpI,kpJ,2)
    if len(matchesLR)<10:
        matches=[]
        M.mwrite(matchLabel,matches)
        print M.Ii,M.Ij," have no consistent point matches"
        return
    matches=M.match(label,8,photometric=photo,geometric={'F':M.F ,'tollerance':rans })
    M.mwrite(matchLabel,matches)
	#im=M.visualize_match(matches,label)
	#imshow(im)
    print M.Ii,M.Ij ,': :',len(matches)


lightD=cv2.ORB(4096,nlevels=32)
lightDl='ORB4k'
lightMl='ORB4kMatches'
filelist=glob.glob('data/images/*.JPG')
filelist=[fn.split('/')[-1] for fn in filelist]
filelist.sort()
filelist=filelist[0:4]
#next time remember to sort the fileNames list
for filename in filelist:
    tab.addImage(filename)

for Im in tab.allImages():
    Im.detect_and_train(lightD,lightDl)

im1=tab.getImage(1)
d_im1=im1.dump()
print d_im1,'\n'
c_im1=ImageBin.load(d_im1)
d2_im1=c_im1.dump()

assert(d2_im1==d_im1)

for M in tab.allMatches():
    matchProcess(M,lightDl,lightMl)

for M in tab.allMatches():
	d_m=M.dump()
	print str(d_m)+'\n'
	c_m=MatchBin.load(tab,d_m)
	d2_m=c_m.dump()
	print bcolors.WARNING+ str(d2_m)+'\n'+ bcolors.ENDC
	assert(d2_m==d_m)

d_tab=tab.dump()
c_tab=tab.load(d_tab)
d2_tab=c_tab.dump()
print d_tab,'\n'
print bcolors.WARNING+ str(d2_tab) +'\n'+ bcolors.ENDC

	
